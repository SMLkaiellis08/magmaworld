
package net.magma_mc.item;

import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.client.event.ModelRegistryEvent;

import net.minecraft.util.ResourceLocation;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.Item;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;

import net.magma_mc.creativetab.TabMagmaWorldtab;
import net.magma_mc.ElementsMagmaWorldMod;

@ElementsMagmaWorldMod.ModElement.Tag
public class ItemBronzeArmor extends ElementsMagmaWorldMod.ModElement {
	@GameRegistry.ObjectHolder("magma_world:bronze_armorhelmet")
	public static final Item helmet = null;
	@GameRegistry.ObjectHolder("magma_world:bronze_armorbody")
	public static final Item body = null;
	@GameRegistry.ObjectHolder("magma_world:bronze_armorlegs")
	public static final Item legs = null;
	@GameRegistry.ObjectHolder("magma_world:bronze_armorboots")
	public static final Item boots = null;
	public ItemBronzeArmor(ElementsMagmaWorldMod instance) {
		super(instance, 15);
	}

	@Override
	public void initElements() {
		ItemArmor.ArmorMaterial enuma = EnumHelper.addArmorMaterial("BRONZE_ARMOR", "magma_world:bronze_", 14, new int[]{2, 6, 7, 2}, 9,
				(net.minecraft.util.SoundEvent) net.minecraft.util.SoundEvent.REGISTRY.getObject(new ResourceLocation("")), 0f);
		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.HEAD).setUnlocalizedName("bronze_armorhelmet")
				.setRegistryName("bronze_armorhelmet").setCreativeTab(TabMagmaWorldtab.tab));
		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.CHEST).setUnlocalizedName("bronze_armorbody")
				.setRegistryName("bronze_armorbody").setCreativeTab(TabMagmaWorldtab.tab));
		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.LEGS).setUnlocalizedName("bronze_armorlegs")
				.setRegistryName("bronze_armorlegs").setCreativeTab(TabMagmaWorldtab.tab));
		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.FEET).setUnlocalizedName("bronze_armorboots")
				.setRegistryName("bronze_armorboots").setCreativeTab(TabMagmaWorldtab.tab));
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerModels(ModelRegistryEvent event) {
		ModelLoader.setCustomModelResourceLocation(helmet, 0, new ModelResourceLocation("magma_world:bronze_armorhelmet", "inventory"));
		ModelLoader.setCustomModelResourceLocation(body, 0, new ModelResourceLocation("magma_world:bronze_armorbody", "inventory"));
		ModelLoader.setCustomModelResourceLocation(legs, 0, new ModelResourceLocation("magma_world:bronze_armorlegs", "inventory"));
		ModelLoader.setCustomModelResourceLocation(boots, 0, new ModelResourceLocation("magma_world:bronze_armorboots", "inventory"));
	}
}
