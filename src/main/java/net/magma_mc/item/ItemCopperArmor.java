
package net.magma_mc.item;

import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.client.event.ModelRegistryEvent;

import net.minecraft.util.ResourceLocation;
import net.minecraft.item.ItemArmor;
import net.minecraft.item.Item;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;

import net.magma_mc.creativetab.TabMagmaWorldtab;
import net.magma_mc.ElementsMagmaWorldMod;

@ElementsMagmaWorldMod.ModElement.Tag
public class ItemCopperArmor extends ElementsMagmaWorldMod.ModElement {
	@GameRegistry.ObjectHolder("magma_world:copper_armorhelmet")
	public static final Item helmet = null;
	@GameRegistry.ObjectHolder("magma_world:copper_armorbody")
	public static final Item body = null;
	@GameRegistry.ObjectHolder("magma_world:copper_armorlegs")
	public static final Item legs = null;
	@GameRegistry.ObjectHolder("magma_world:copper_armorboots")
	public static final Item boots = null;
	public ItemCopperArmor(ElementsMagmaWorldMod instance) {
		super(instance, 39);
	}

	@Override
	public void initElements() {
		ItemArmor.ArmorMaterial enuma = EnumHelper.addArmorMaterial("COPPER_ARMOR", "magma_world:copper_", 14, new int[]{2, 4, 5, 2}, 8,
				(net.minecraft.util.SoundEvent) net.minecraft.util.SoundEvent.REGISTRY.getObject(new ResourceLocation("")), 0f);
		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.HEAD).setUnlocalizedName("copper_armorhelmet")
				.setRegistryName("copper_armorhelmet").setCreativeTab(TabMagmaWorldtab.tab));
		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.CHEST).setUnlocalizedName("copper_armorbody")
				.setRegistryName("copper_armorbody").setCreativeTab(TabMagmaWorldtab.tab));
		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.LEGS).setUnlocalizedName("copper_armorlegs")
				.setRegistryName("copper_armorlegs").setCreativeTab(TabMagmaWorldtab.tab));
		elements.items.add(() -> new ItemArmor(enuma, 0, EntityEquipmentSlot.FEET).setUnlocalizedName("copper_armorboots")
				.setRegistryName("copper_armorboots").setCreativeTab(TabMagmaWorldtab.tab));
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerModels(ModelRegistryEvent event) {
		ModelLoader.setCustomModelResourceLocation(helmet, 0, new ModelResourceLocation("magma_world:copper_armorhelmet", "inventory"));
		ModelLoader.setCustomModelResourceLocation(body, 0, new ModelResourceLocation("magma_world:copper_armorbody", "inventory"));
		ModelLoader.setCustomModelResourceLocation(legs, 0, new ModelResourceLocation("magma_world:copper_armorlegs", "inventory"));
		ModelLoader.setCustomModelResourceLocation(boots, 0, new ModelResourceLocation("magma_world:copper_armorboots", "inventory"));
	}
}
