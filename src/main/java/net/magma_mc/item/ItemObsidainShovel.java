
package net.magma_mc.item;

import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.common.util.EnumHelper;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.client.event.ModelRegistryEvent;

import net.minecraft.item.ItemStack;
import net.minecraft.item.ItemSpade;
import net.minecraft.item.Item;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;

import net.magma_mc.creativetab.TabMagmaWorldtab;
import net.magma_mc.ElementsMagmaWorldMod;

import java.util.Set;
import java.util.HashMap;

@ElementsMagmaWorldMod.ModElement.Tag
public class ItemObsidainShovel extends ElementsMagmaWorldMod.ModElement {
	@GameRegistry.ObjectHolder("magma_world:obsidain_shovel")
	public static final Item block = null;
	public ItemObsidainShovel(ElementsMagmaWorldMod instance) {
		super(instance, 37);
	}

	@Override
	public void initElements() {
		elements.items.add(() -> new ItemSpade(EnumHelper.addToolMaterial("OBSIDAIN_SHOVEL", 8, 1741, 14f, 1f, 56)) {
			{
				this.attackSpeed = -3f;
			}
			public Set<String> getToolClasses(ItemStack stack) {
				HashMap<String, Integer> ret = new HashMap<String, Integer>();
				ret.put("spade", 8);
				return ret.keySet();
			}
		}.setUnlocalizedName("obsidain_shovel").setRegistryName("obsidain_shovel").setCreativeTab(TabMagmaWorldtab.tab));
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void registerModels(ModelRegistryEvent event) {
		ModelLoader.setCustomModelResourceLocation(block, 0, new ModelResourceLocation("magma_world:obsidain_shovel", "inventory"));
	}
}
