
package net.magma_mc.item.crafting;

import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;

import net.minecraft.item.ItemStack;

import net.magma_mc.item.ItemBronzeIngot;
import net.magma_mc.block.BlockBronzeore;
import net.magma_mc.ElementsMagmaWorldMod;

@ElementsMagmaWorldMod.ModElement.Tag
public class RecipeSmeltBronzeOre extends ElementsMagmaWorldMod.ModElement {
	public RecipeSmeltBronzeOre(ElementsMagmaWorldMod instance) {
		super(instance, 52);
	}

	@Override
	public void init(FMLInitializationEvent event) {
		GameRegistry.addSmelting(new ItemStack(BlockBronzeore.block, (int) (1)), new ItemStack(ItemBronzeIngot.block, (int) (1)), 1F);
	}
}
