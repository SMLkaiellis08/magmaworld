
package net.magma_mc.item.crafting;

import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;

import net.minecraft.item.ItemStack;

import net.magma_mc.item.ItemCopperIngot;
import net.magma_mc.block.BlockCopperOre;
import net.magma_mc.ElementsMagmaWorldMod;

@ElementsMagmaWorldMod.ModElement.Tag
public class RecipeSmeltCopperOre extends ElementsMagmaWorldMod.ModElement {
	public RecipeSmeltCopperOre(ElementsMagmaWorldMod instance) {
		super(instance, 101);
	}

	@Override
	public void init(FMLInitializationEvent event) {
		GameRegistry.addSmelting(new ItemStack(BlockCopperOre.block, (int) (1)), new ItemStack(ItemCopperIngot.block, (int) (1)), 1F);
	}
}
