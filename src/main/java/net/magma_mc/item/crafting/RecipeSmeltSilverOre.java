
package net.magma_mc.item.crafting;

import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;

import net.minecraft.item.ItemStack;

import net.magma_mc.item.ItemSilverIngot;
import net.magma_mc.block.BlockSilverOre;
import net.magma_mc.ElementsMagmaWorldMod;

@ElementsMagmaWorldMod.ModElement.Tag
public class RecipeSmeltSilverOre extends ElementsMagmaWorldMod.ModElement {
	public RecipeSmeltSilverOre(ElementsMagmaWorldMod instance) {
		super(instance, 53);
	}

	@Override
	public void init(FMLInitializationEvent event) {
		GameRegistry.addSmelting(new ItemStack(BlockSilverOre.block, (int) (1)), new ItemStack(ItemSilverIngot.block, (int) (1)), 1F);
	}
}
