
package net.magma_mc.item.crafting;

import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;

import net.minecraft.item.ItemStack;

import net.magma_mc.item.ItemTinIngot;
import net.magma_mc.block.BlockTinOre;
import net.magma_mc.ElementsMagmaWorldMod;

@ElementsMagmaWorldMod.ModElement.Tag
public class RecipeSmeltTinORE extends ElementsMagmaWorldMod.ModElement {
	public RecipeSmeltTinORE(ElementsMagmaWorldMod instance) {
		super(instance, 109);
	}

	@Override
	public void init(FMLInitializationEvent event) {
		GameRegistry.addSmelting(new ItemStack(BlockTinOre.block, (int) (1)), new ItemStack(ItemTinIngot.block, (int) (1)), 1F);
	}
}
