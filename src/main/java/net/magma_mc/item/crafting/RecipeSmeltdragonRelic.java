
package net.magma_mc.item.crafting;

import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;

import net.minecraft.item.ItemStack;

import net.magma_mc.item.ItemDragonRelicIngot;
import net.magma_mc.block.BlockDragonRelic;
import net.magma_mc.ElementsMagmaWorldMod;

@ElementsMagmaWorldMod.ModElement.Tag
public class RecipeSmeltdragonRelic extends ElementsMagmaWorldMod.ModElement {
	public RecipeSmeltdragonRelic(ElementsMagmaWorldMod instance) {
		super(instance, 54);
	}

	@Override
	public void init(FMLInitializationEvent event) {
		GameRegistry.addSmelting(new ItemStack(BlockDragonRelic.block, (int) (1)), new ItemStack(ItemDragonRelicIngot.block, (int) (1)), 1F);
	}
}
