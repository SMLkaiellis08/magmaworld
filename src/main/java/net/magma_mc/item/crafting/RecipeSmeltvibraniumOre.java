
package net.magma_mc.item.crafting;

import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;

import net.minecraft.item.ItemStack;

import net.magma_mc.item.ItemVibraniumIngot;
import net.magma_mc.block.BlockVibraniumOre;
import net.magma_mc.ElementsMagmaWorldMod;

@ElementsMagmaWorldMod.ModElement.Tag
public class RecipeSmeltvibraniumOre extends ElementsMagmaWorldMod.ModElement {
	public RecipeSmeltvibraniumOre(ElementsMagmaWorldMod instance) {
		super(instance, 108);
	}

	@Override
	public void init(FMLInitializationEvent event) {
		GameRegistry.addSmelting(new ItemStack(BlockVibraniumOre.block, (int) (1)), new ItemStack(ItemVibraniumIngot.block, (int) (1)), 1F);
	}
}
