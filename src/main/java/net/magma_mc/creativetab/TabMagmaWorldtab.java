
package net.magma_mc.creativetab;

import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.fml.relauncher.Side;

import net.minecraft.item.ItemStack;
import net.minecraft.creativetab.CreativeTabs;

import net.magma_mc.item.ItemObsidainIngot;
import net.magma_mc.ElementsMagmaWorldMod;

@ElementsMagmaWorldMod.ModElement.Tag
public class TabMagmaWorldtab extends ElementsMagmaWorldMod.ModElement {
	public TabMagmaWorldtab(ElementsMagmaWorldMod instance) {
		super(instance, 64);
	}

	@Override
	public void initElements() {
		tab = new CreativeTabs("tabmagma_worldtab") {
			@SideOnly(Side.CLIENT)
			@Override
			public ItemStack getTabIconItem() {
				return new ItemStack(ItemObsidainIngot.block, (int) (1));
			}

			@SideOnly(Side.CLIENT)
			public boolean hasSearchBar() {
				return true;
			}
		}.setBackgroundImageName("item_search.png");
	}
	public static CreativeTabs tab;
}
